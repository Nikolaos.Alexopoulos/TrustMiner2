#!/bin/bash

ima=$1

test -f "$1" || echo "Unable to open IMA measurement file in argument #1: '$1'"
test -f "$1" || exit

echo "Cleaning up IMA measurement file.."

sed -i '/.*\.conf$/d' $ima 
sed -i '/.*\.cnf$/d' $ima 
sed -i '/.*\.cfg$/d' $ima 
sed -i '/.*\.png$/d' $ima 
sed -i '/.*\.jpg$/d' $ima 
sed -i '/.*\.jpeg$/d' $ima 
sed -i '/.*\.mp3$/d' $ima 
sed -i '/.*\.list$/d' $ima 
sed -i '/.*\.deb$/d' $ima 
sed -i '/.*\.IndexDiff$/d' $ima 
sed -i '/.*\.c$/d' $ima 
sed -i '/.*\.o$/d' $ima 
sed -i '/.*\.h$/d' $ima 
sed -i '/.*\.pyo$/d' $ima 
sed -i '/.*\.tex$/d' $ima 
sed -i '/.*\.cache$/d' $ima 
sed -i '/.*\.prerm$/d' $ima 
sed -i '/.*\.postrm$/d' $ima 
sed -i '/.*\.vim$/d' $ima 
sed -i '/.*\.tfm$/d' $ima 
sed -i '/.*\.postinst$/d' $ima 
sed -i '/.*\.pyc$/d' $ima 
sed -i '/.*\.leases$/d' $ima 
sed -i '/.*\.hook$/d' $ima 
sed -i '/.*\_Packages$/d' $ima 
sed -i '/.*\.gz$/d' $ima 
sed -i '/.*\.bz2$/d' $ima 
sed -i '/.*\.s$/d' $ima 
sed -i '/.*\.d$/d' $ima 
sed -i '/.*\.rules$/d' $ima 
sed -i '/.*\.log$/d' $ima 
sed -i '/.*\.bin$/d' $ima 
sed -i '/.*\/preinst$/d' $ima 
sed -i '/.*\.dpkg-old$/d' $ima 
sed -i '/.*\.dpkg-new$/d' $ima 
