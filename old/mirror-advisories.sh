#!/bin/bash

# init values for when to start fetching - auto-updated by $config below
dsa=2202
usn=1179

mirror=/srv/http/htdocs/apt-sec/mirror
config=$mirror/advisory_states.conf
. $config


echo "Mirroring advisories..."

## Debian
#rm -f "$mirror/dsa/2011/dsa-$(printf "%03d" $dsa)" || true; 
while (wget -cqx -P "$mirror/dsa/" -nH --cut-dirs=1 "http://www.debian.org/security/2012/dsa-$(printf "%03d" $dsa)"); do
	echo "Got Debian DSA $dsa"
	let dsa++;
done
cd ..

## Ubuntu
#rm "$mirror/usn/usn-$(printf "%03d" $usn)-1"
while (wget -cqx -P "$mirror/usn" -nH --cut-dirs=1 "http://www.ubuntu.com/usn/usn-$usn-1"); do
	echo "Got Ubuntu USN $usn"
	let usn++;
done

# save new dsa/usn status
(echo dsa=$dsa; echo usn=$usn ) > $config

# mirror all FreeBSD advisories
# TODO: make this more clever so we don't touch everything all the time..
#wget -P "$mirror/fsa" -nH --cut-dirs=1 -rc -A '*.asc' -l 1 http://security.freebsd.org/advisories/ 

