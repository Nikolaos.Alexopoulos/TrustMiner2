#!/bin/bash

## Iterate through complete package archive to generate a complete list of SHA1
## command parameter 'init' will build from scratch. Very slow.

root=/srv/apt-mirror
archive=$root/mirror
dest=/srv/http/htdocs/apt-sec/mirror/Sha1Sums
var=$root/var
NEW=$var/NEW
ALL=$var/ALL

action="$1"


echo "Updating Sha1Sums.."

shalist () {
	pkg="$1"
	tmp=$(mktemp -d)
	ar -t $pkg |grep -q data.tar.gz && ( ar p $pkg data.tar.gz | tar xzC $tmp || echo "Can't extract from $pkg" )
	ar -t $pkg |grep -q data.tar.bz2 && ( ar p $pkg data.tar.bz2 | tar xjC $tmp || echo "Can't extract from $pkg" )
	cd $tmp
	find . -type f -print0 |xargs -0 sha1sum |tee -a $dest.paths | cut -c -42|sed -e "s/ .*/::$(basename $pkg)/" >> $dest.pkgs
	cd /
	rm -rf $tmp || (echo Trying even harder...; chmod -R u+rwx $tmp; rm -rf $tmp)
}

case $action in

"init")
	# delete and rebuild shalist 
	echo "Reinit - deleting current SHA1 sums.."
	rm $dest.pkgs
	rm $dest.paths

	echo "Reinitializing SHA1 DB.."
	n=0
	for deb in $(find $archive -type f -iname \*.deb); do
	 	let n++
		printf "%5d\t$deb\n" $n
		shalist $deb
	done
	;;

"update")
	# record removed packets, remove sha1sums of packets recorded *last week*

	# delete packages from last week
	del=$var/DEL-$(date +%w)
	touch $del

	grep -v -F -f $del $dest.pkgs > $dest.pkgs.tmp
	mv $dest.pkgs.tmp $dest.pkgs
	echo -n > $del

	# record newly removed packages, overwriting last week's record
	for pkg in $(cut -c 43- $dest.pkgs|uniq); do
		grep -q \\/$pkg$ $ALL || echo $pkg >> $del
	done

	# add sha sums for new pkgs
	for deb in $(cat $NEW); do
	 	deb=$(echo $deb|sed 's/.*:\/\/'//);
		echo "Adding $pkg.."
		shalist $archive/$deb
	done
	# delete, so we don't use them again
	# (hopefully, apt-mirror does not need it...?)
	echo -n > $NEW
	;;
"nosha1")
	echo > $dest.pkgs
	echo > $dest.paths
	;;
*)
	echo
	echo "Usage: $0 (init|update)"
	echo
	exit
esac	

ln $dest.pkgs $dest
bzip2 -f $dest
