#!/bin/bash

dest=/srv/http/htdocs/apt-sec/mirror/cve

# call apt-sec from different pwd so that it uses different config and
# caching files in this mirroring step
dir=/srv/http/htdocs/apt-sec
aptsec=$dir/apt-sec


echo "Extracting CVEs..."

cd $dir/mirror

log="$($aptsec update --cves)"
#echo "$log"

urls="$(echo "$log"|grep '^NeedCVE:'|grep -v 'LOCAL-'|cut -b 10-)"

# don't suck any more than we have to..
urls="$(echo "$urls"|sort |uniq|sort -n)"

for url in $urls; do
	echo -n "$($url|sed 's/.*=//') "
done

[ "AA$urls" = "AA" ] && exit

echo "Mirroring CVEs..."

for url in $urls; do
	name="$(echo $url|sed 's/.*?//')"
	file="$dest/$name"

	if [ -f "$file" ]; then
		echo "Already got ${name##*=} (?!)"
		continue
	else
		wget -qO "$file" "$url" || echo "Error retrieving $url.."
	fi

	# do some verification and delete bad downloads
	# XXX replicates apt-sec parsing code. instead, we should
	#	post-process and provide proper input to apt-sec..
	basename $file|grep -q '^vulnId=' && grep -q "is valid CVE format, but CVE was not found" "$file" && rm "$file"
	basename $file|grep -q '^name=' && grep -q "Could not find a CVE entry or candidate named" "$file" && rm "$file"
done
