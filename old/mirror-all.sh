#!/bin/bash -e


##
## Wrapper around mirroring scripts.
##
## Currently we do all Debian distributions.
## To mirror a specific one, change apt-mirror configuration and filter the
## types of Packages.bz2 and Sources.bz2 files to include.
##

dir=/srv/http/htdocs/apt-sec
action="$1"

# echo "Waiting for lock.."
# lockfile-create /var/lock/aptsec-mirror
# lockfile-touch /var/lock/aptsec-mirror &
# BADGER="$!"

echo -n "$(date +%x\ %R): "
echo "Launching main apt-sec mirroring"

if [ "A$action" = "Ainit" ]; then
	echo "Re-initializing mirror state.."
	rm -f $dir/mirror/cache/*
fi


apt-mirror|grep -v "^Proceed indexes\|\]\.\.\.\|^Removing \|^Begin time:\|^End time:"

echo -n "$(date +%x\ %R): "
echo "Done synching package data."

$dir/gen-sha1-list.sh $action|sed s/"^Adding .*deb.."/"Packets added"/|uniq -c
echo
$dir/gen-pkg-lists.sh $action

echo -n "$(date +%x\ %R): "
echo "Done synching meta data"

$dir/mirror-advisories.sh
$dir/mirror-cves.sh

echo -n "$(date +%x\ %R): "
echo "Done syncing advisory data"

# kill "${BADGER}"
# lockfile-remove /var/lock/aptsec-mirror
