#!/bin/bash

# wrapper around update scripts to produce shorter and more useful email reports
# TODO: Maybe better implemented with different log levels..

ROOT=/srv/http/htdocs/apt-sec/

mirror=$($ROOT/mirror-all.sh update)
aptsec=$($ROOT/update-aptsec.sh)

echo "$mirror" > $ROOT/cronjob.log
echo "$aptsec" >> $ROOT/cronjob.log

# first uniq summarizes each bad file into packages,
# second uniq -c counts number of bad packages for cron report
head=$(echo "$aptsec" |grep -B 500 "^Parsing Sha1Sums file...$")
pkgs=$(echo "$aptsec" |grep "^No pkg known" |sort |uniq |sed s/.*/"pkg(s) not found (stale sha1sum entry?)"/ |uniq -c)
spkg=$(echo "$aptsec" |grep "^No srcpkg known" |sort |uniq |sed s/.*/"srcpkg(s) not found (inconsistent meta-data?)"/ |uniq -c)
done=$(echo "$aptsec" |grep "^Processing package" |sed s/.*/"Packages processed"/ |uniq -c) 
end=$(echo  "$aptsec" |grep -A 10000 "^Parsing Sha1Sums file...$" \
									    |grep -v "^Parsing Sha1Sums file...$"|grep -v "^Processing" \
										 	|grep -v "^No pkg known for"|grep -v "^No srcpkg known for")

# must re-create graphs
rm $ROOT/html/graphs/*.png

# output for email report
echo "$mirror"

echo "$head"
echo "$pkgs"
echo "$spkg"
echo "$done"
echo "$end"
