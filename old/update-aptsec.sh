#!/bin/bash -e

dir=/srv/http/htdocs/apt-sec
action="$1"

cd $dir

[ "A$action" = "Ainit" ] && rm -f $dir/cache/* || true

# signal that we're updating apt-sec cache
lockfile-create /var/lock/aptsec-update
lockfile-touch /var/lock/aptsec-update &
BADGER2="$!"
./apt-sec update |grep -v ^Adding |tee apt-sec.log

echo -n "$(date +%x\ %R): Done synching apt-sec caches"


kill "${BADGER2}"
lockfile-remove /var/lock/aptsec-update

