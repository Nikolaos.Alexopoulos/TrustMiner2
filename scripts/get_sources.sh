#!/bin/bash
echo Start: > sources_server.txt
while read p; do
	if [[ $p == *:i386 ]]
	then
		p=${p%:i386}
	elif [[ $p == *:amd64 ]]
	then
		p=${p%:amd64}
	fi
	echo $p
	apt-cache showsrc $p | grep Package: >> sources_server.txt
done <dev_server.lst
