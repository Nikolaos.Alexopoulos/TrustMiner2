#!/usr/bin/python3

import csv

def create_trust_file(filename):
    with open('sources_carlos_final.txt', mode='r') as infile:
        reader = csv.reader(infile)
        mysources = dict()
        for row in reader:
            key = row[0]
            mysources[key] = row[1:]

    with open(filename, mode='r') as infile:
        reader = csv.reader(infile)
        allpreds = dict()
        for row in reader:
            key = row[0]
            allpreds[key] = row[1:]
        
    mypreds = dict()
    total_preds = 0
    total_real = 0
    for i in mysources:
        if i in allpreds:
            mypreds[i] = allpreds[i]
            pred = float(allpreds[i][0])
            if (pred < 0):
                pred = 0.0
            real = float(allpreds[i][1])
            total_preds += pred
            total_real += real

    print(mypreds)
    print(total_preds, total_real)

if __name__ == '__main__':
    create_trust_file('output-Errors6smoothing6neurons10.txt')
