#!/usr/bin/python3
import apt

def getdeps(srcpkg, src2deps):
    
    src2deps[srcpkg] = []

    cache = apt.Cache()
    counter = 0

    for binpkg in cache:
        if binpkg.versions[0].source_name == srcpkg:
            deps = binpkg.versions[0].get_dependencies('PreDepends', 'Depends')
            for dep in deps:
                try:
                    srcdep = cache[dep.or_dependencies[0].name].versions[0].source_name
                except KeyError:
                    srcdep = 'package does not exist currently'

                if not (srcdep in src2deps[srcpkg]):
                    src2deps[srcpkg].append(srcdep)
    return 0
