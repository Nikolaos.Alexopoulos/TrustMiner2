from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
from scipy import stats
from statsmodels.graphics.api import qqplot
#from keras.models import Sequential
#from keras.layers import Dense, Activation, Dropout
#from keras.layers.recurrent import LSTM
import statsmodels.api as sm

def predict(rawdata):
    np.random.seed(1234)
    i = 0
    while rawdata[i] == 0:
        i += 1

    indexes = []
    year = 1995

    print(rawdata)
    for i in range(len(rawdata)):
        indexes[i] = str(year) 

    usefuldata = rawdata[i:len(rawdata)-1]
    print(usefuldata)
    
    dta = pd.Series(usefuldata)
    dta = dta.astype(float)
    print(dta)
    dta.plot(figsize=(12,8))
    fig = plt.figure(figsize=(12,8))
    ax1 = fig.add_subplot(211)
    fig = sm.graphics.tsa.plot_acf(dta.values.squeeze(), lags=20, ax=ax1)
    ax2 = fig.add_subplot(212)
    fig = sm.graphics.tsa.plot_pacf(dta, lags=20, ax=ax2)

    arma_mod20 = sm.tsa.ARMA(dta, (2,0)).fit()
    print(arma_mod20.params)
    
    arma_mod30 = sm.tsa.ARMA(dta, (3,0)).fit()
    print(arma_mod20.aic, arma_mod20.bic, arma_mod20.hqic)

    print(arma_mod30.params)
    print(arma_mod30.aic, arma_mod30.bic, arma_mod30.hqic)

    sm.stats.durbin_watson(arma_mod30.resid.values)

#    fig = plt.figure(figsize=(12,8))
#    ax = fig.add_subplot(111)
#    ax = arma_mod20.resid.plot(ax=ax);



#    fig = plt.figure(figsize=(12,8))
#    ax = fig.add_subplot(111)
#    ax = arma_mod30.resid.plot(ax=ax);

    resid = arma_mod30.resid

#    fig = plt.figure(figsize=(12,8))
#    ax = fig.add_subplot(111)
#    fig = qqplot(resid, line='q', ax=ax, fit=True)
    print('this is the data indexes ' + str(dta.ix[0]))

    fig, ax = plt.subplots(figsize=(12, 8))
    ax = dta.ix['0':].plot(ax=ax)
    fig = arma_mod30.plot_predict('29', '30', dynamic=True, ax=ax, plot_insample=False)
    

    plt.show()

