import powerlaw
from numpy import genfromtxt
import matplotlib.pyplot as plt


mydata = genfromtxt('power_law.csv', delimiter=',', dtype = 'int')
print(len(mydata))
print(mydata)

## Build and print probability distribution, bins per 10
distr = dict()
for i in mydata:
    bins = i // 10
    if bins in distr:
        distr[bins] += 1
    else:
        distr[bins] = 1

for i in distr:
    print(str(i) + ', ' + str(distr[i]))


results=powerlaw.Fit(mydata, True, xmin=1, xmax = 2000)
print(results.power_law.alpha)
print(results.truncated_power_law.alpha)
print(results.power_law.xmin)
print(results.truncated_power_law.xmin)
print(results.truncated_power_law.xmax)
print(results.power_law.discrete)
print(results.lognormal.mu)
results.plot_pdf(color = 'blue')
results.power_law.plot_pdf(color = 'green')
results.truncated_power_law.plot_pdf(color = 'red')
#plt.plot(results.data)
plt.show()
R, p=results.distribution_compare('power_law','exponential')
print(R,p)
