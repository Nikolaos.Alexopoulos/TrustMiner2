#!/bin/bash

HTTP_HEADER='HTTP/1.1 200 OK
Connection: close
Content-Type: text/html; charset=utf-8
'

#<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
#<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">

HTML_HEADER='
<html>
<head>
<title>TrustMiner</title>
<link rel="stylesheet" title="nuclblog" type="text/css" href="'"$URL/html/aptsec.css"'"
</head>
<body>
<br>
<h1>TrustMiner - Mining Software for Trust</h1>
<p /><br>
'

MAIN_NAVI='<div class="navi">
<ul>
<a href="'"$URL/intro"'"> <li>Introduction</li></a>
<a href="'"$URL/sys"'">   <li>System Assessment</li></a>
<a href="'"$URL/pkg"'">   <li>Database Query</li></a>
<a href="'"$URL/hits"'">  <li>High Scores</li></a>
<a href="'"$URL/about"'"> <li>About</li></a>
</ul>
</div>'

HTML_FOOTER='
<div class="footer">
© 2011 Sven Bugiel, Lucas Davi, Ahmad-Reza Sadeghi, Steffen Schulz.
<a href="http://www.trust.cased.de">System Security Lab, TU Darmstadt</a>, Germany. All rights reserved.<br>
Published as "<a href="http://www.trust.informatik.tu-darmstadt.de/fileadmin/user_upload/Group_TRUST/PubsPDF/stc6032-bugiel.pdf"
>Scalable Trust Establishment with Software Reputation</a>", ACM Workshop on Scalable Trusted Computing (STC), 2011
</div>
</body></html>'
			
FILE_FORM='
<form action="'"$URL"'/sys" method="post" enctype="multipart/form-data">
<p>To evaluate a system, please upload a Debian DPKG status file (/var/lib/dpkg/status)
or a IMA measurements file (/sys/kernel/security/ima/ascii_runtime_measurements).</p>
<input name="file" type="file" size="50">
<input type="submit">
</form>'

#You can manually query the database by entering binary or source package names,
#a SHA-1 measurement of a package file or an CVE and DSA identifier:
OBJECT_FORM='
<form action="'"$URL"'/html/index.sh" method="get">
Enter binary or source package names to manually query the database.
Examples: mplayer, vlc, apache2, lighttpd, linux-2.6.

<input name="action" value="pkg" type="hidden">
<input name="object" type="text" size="50">
<input type="submit">
</form>'

LAMBDA='<sub>&#955;</sub>'

INTRO_MSG='
TrustMiner is a tool for estimating the trustworthiness of software based on
past security incidents. The current prototype uses the Debian GNU/Linux
software repositories and security incidents database to provide security
assessments for Debian software packages and systems.

The purpose of this tool is to estimate the trustworthiness (or security
failure rate) of a given computer system (prover), as shown in the figure
below. This website implements the <em>verification</em> or <em>assessment</em> part of the
figure, and the client (you) can submit information to be evaluated.

<img width="600px" src='"$URL"'/html/debian_trustworthiness.png></img>
'

ABOUT_REF='
The database is updated daily based on new Debian Security Announcements
and Debian package repositories. We mirror the current stable, unstable and
testing distributions for the official Debian package repository as well as
debian-multimedia and security updates repositories. The SHA-1 hash sums of
packages deleted from the official repositories are kept for one week.

Note that we do not consider whether your system has all the current security
patches installed, or if unpatched vulnerabilities are known for your system.
This information is already provided by <a
href="http://www.enyo.de/fw/software/debsecan/">debsecan</a> and on the Debian website:
<ul>
<li>Packages with available updates: <em>debsecan --only-fixed --suite sid</em>
<li><a href="http://security-tracker.debian.org/tracker/status/release/unstable">Unpatched packages in the unstable suite</a>
<li><a href="http://security-tracker.debian.org/tracker/status/release/testing">Unpatched packages in the testing suite</a>
<li><a href="http://security-tracker.debian.org/tracker/status/release/stable">Unpatched packages in the stable suite</a>
</ul>
For further information, see:
<ul><li><a href="http://www.trust.informatik.tu-darmstadt.de/fileadmin/user_upload/Group_TRUST/PubsPDF/stc6032-bugiel.pdf">Scalable Trust Establishment with Software Reputation</a>, ACM STC, 2011
<li><a href="http://www.debian.org/security/">Debian Security Information</a>
<li><a href="http://security-tracker.debian.org/tracker/">Debian Security Tracker</a>
</ul>
'

UPDATE_MSG='
<h2>Sync in progress...</h2>

TrustMiner is currently processing new advisories and packages.
Sorry for the inconvenience.
'
