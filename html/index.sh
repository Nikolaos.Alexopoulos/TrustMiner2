#!/bin/bash -E 

URL="/apt-sec"
sURL="\/apt-sec"
WD="/srv/http/htdocs/apt-sec/html"

. pieces.sh
. libcgi2.sh



##
## do the work
##
cgi_getvars GET action
cgi_getvars GET object
#cgi_getvars BOTH file

## debugging
# echo '<pre>'
# set |sed 's/</\&lt\;/';
# env |sed 's/</\&lt\;/';
# echo '</pre>'

## input variable cavity search
object=$(echo "$object"|tr -d "<>;|#\"'")
action=$(echo "$action"|tr -d "<>;|#\"'")

# disable during aptsec mirroring..
if lockfile-check /var/lock/aptsec-update; then
	output="$UPDATE_MSG"
else

case $action in
	"sys")
		if [ "$REQUEST_METHOD" != "POST" ]; then
			output="<h2>System Assessment</h2><p>$FILE_FORM</p>"
		else
			tmp="$(tempfile)";
			# crude hack: delete last 2 and first 4 lines to extract binary file
			#echo -n "$QUERY_STRING_POST"|tac|tail -n +3|tac|tail -n +5 > $tmp
			cat - |tac|tail -n +3|tac|tail -n +5 > $tmp

			if grep -q 'boot_aggregate' "$tmp"; then
				output="$(../apt-sec attest $tmp)"
			else
				output="$(../apt-sec status $tmp)"
			fi
			rm $tmpfile

			sum=$(echo "$output"|grep -A99 ^Packages\ with)
			lines=$(($(echo "$output"|wc -l|awk '{print $1}')-$(echo "$sum"|wc -l|awk '{print $1}')))
			log=$(echo -e "\n\nAssessment details:"; echo "$output"|head -$lines)
			output="$(echo "$sum$log"|sed "s/MTTFl/MTTF<sub>\&#955;<\/sub>/")"
			output="<h2>System Assessment (&#955=36)</h2><p>$output</p>"
		fi
		;;
	"jmp")
		echo "$HTTP_HEADER"
		echo '<html><head><meta http-equiv="refresh" content="0;url='

		case $object in
			"DSA-"*)
				echo -n 'http://security-tracker.debian.org/tracker/redirect/'
				echo -n "$object"
				;;
			"CVE-"*)
				echo -n 'http://security-tracker.debian.org/tracker/redirect/'
				echo -n "$object"
				;;
			*)
				## assume its a debian packet
				echo -n 'http://packages.debian.org/'
				echo -n "$object"
				;;
		esac
		echo '"></head><body></body></html>'
		exit
		;;
	"pkg")
		case $object in
			"")
				output="<h2>Manual Database Query</h2><p>$OBJECT_FORM</p>"
				;;
			*)
				#echo "AA${object}AA"
				#echo -n "$object"|wc -c
				#[[ "$object" =~ [a-z,A-Z,0-9] ]] && echo true;
				if [ $(echo -n "$object"|wc -c) -eq 40 ] && [[ "$object" =~ [a-z,A-Z,0-9] ]]; then
					output="$(../apt-sec sha1 $object)"
					output="<h2>Records on SHA1 digest $object</h2>$output"
				else
					output="$(../apt-sec show $object)"
					alt="$(../apt-sec alt $object)"

					msg="$(echo "$output"|grep ^Resolving)" && msg="$(echo -e "$msg\r\r")"
					output=$(echo "$output"|grep -v ^Resolving)

					pkgh="$(echo "$output"|awk -F'\n' '{ RS=""; if (NR == 2) {print "<h3>"$0"</h3>"}}')"
					pkg="$(echo  "$output"|awk -F'\n' '{ RS=""; if (NR == 3) {print }}')"
					logh="$(echo "$output"|awk -F'\n' '{ RS=""; if (NR == 4) {print "\r\r<h3>",$0,"</h3>"}}')"
					log="$(echo  "$output"|awk -F'\n' '{ RS=""; if (NR == 5) {print }}')"
					sumh="$(echo "$output"|awk -F'\n' '{ RS=""; if (NR == 6) {print "\r<h3>",$0,"</h3>"}}')"
					sum="$(echo  "$output"|awk -F'\n' '{ RS=""; if (NR == 7) {print }}')"

					sum="$(echo "$sum"|grep -v "MTTFl\|STPl" \
						|sed -e "s/MTTF:       /<i>Mean Time To Failure:<\/i>/" \
						     -e "s/STP(12):  /<i>Failure prediction:<\/i>/")"
					#sum="$(echo "$sum"|sed -e "s/MTTFl/MTTF<sub>\&#955;<\/sub>/" -e "s/STPl/STP<sub>\&#955;<\/sub>/")"
					log="$(echo "$log"|sed 's/^DSA-[0-9]\+/<br><a href="\/apt-sec\/jmp\/&">&<\/a>/')"
					log="$(echo "$log"|sed 's/^CVE-[0-9]\+-[0-9]\+/<a href="\/apt-sec\/jmp\/&">&<\/a>/')"
					pkg="$(echo "$pkg"|sed 's/.*/<a href="\/apt-sec\/jmp\/&">&<\/a>/')"
					
					alth="$(echo "$alt"|grep ^Similar |sed -e 's/^/<br><h3>/' -e 's/$/<\/h3>/')"
					alts="$(echo "$alt"|tail -2|sed -e "s/\([a-z,A-Z,0-9,\._-]*\)\ /<a href=\"$sURL\/pkg\/\1\">\1<\/a><br>/g")"

					graph="$WD/graphs/$(echo $object|md5sum|cut -b -16)"
					if [ ! -f $graph.png ]; then
						echo "$log"|grep CVE |sed s/,//g | awk '{print $5"."$6}' \
							|awk 'FS="." {print $5"-"$4"-"$3,$1"."$2}' |sort |grep -v -- '^--'> $graph.dat
						gnuplot cvegraph.plot -e "plot '$graph.dat' using 1:2 with impulses smooth frequency" > $graph.png
					fi
					img="<p><img src=\"$URL/html/graphs/$(basename $graph).png\"></img>"

					output="<h2>Records on package $object</h2>$img$msg$sumh$sum$alth$alts$logh$log$pkgh$pkg"
				fi
				;;
		esac
		;;
	"hits")
		output="$(../apt-sec hits|sed \
			-e "s/MTTFl/MTTF<sub>\&#955;<\/sub>/" \
			-e "s/STPl/STP<sub>\&#955;<\/sub>/" \
			-e "s/Pkg: \(.*\)/<a href=\"$sURL\/pkg\/\1\">\1<\/a>/" \
			|sort -n|head -100)"
		output="<h2>Worst 100 Packages (sorted by MTTF, &#955=36)</h2><p>$output</p>"
		;;
	"about")
		output="$(../apt-sec about)"
		output="<h2>About TrustMiner</h2>$ABOUT_MSG$output$ABOUT_REF"
		;;
	*)
		#"intro")
		output="<h2>Introduction</h2>$INTRO_MSG"
		;;
esac

fi # disable on aptsec mirroring..

##
## format HTML output
##
echo "$HTTP_HEADER"
echo "$HTML_HEADER"

echo "$MAIN_NAVI"

echo '<div class="output">'
echo "$output"
echo '</div>'
echo '<div class="padding">&nbsp;</div>'

echo "$HTML_FOOTER"
