set nokey
set title "CVE History of Package"
set xlabel "Assignment Date"
set ylabel "Accumulated Severity"

set xdata time
set timefmt "%Y-%m-%d"
set xtics nomirror rotate by -60
set rmargin 4
set xrange ["2004-1-1":]

set size ratio 0.3
set terminal png 12

#plot '-' using 1:2 with boxes smooth frequency
