#! /bin/bash -E
 
# (internal) routine to store POST data
function cgi_get_POST_vars() {
    # save POST variables (only first time this is called)
    [ -z "$QUERY_STRING_POST" \
      -a "$REQUEST_METHOD" = "POST" -a ! -z "$CONTENT_LENGTH" ] && \
        read -N $CONTENT_LENGTH QUERY_STRING_POST

		if [ "${CONTENT_TYPE}" != "application/x-www-form-urlencoded" ]; then
    		# might work with regular parsing below..
        return
		elif [ "${CONTENT_TYPE}" != "multipart/form-data" ]; then
			# cant handle..
			return
    fi
    return
}
 
# (internal) routine to decode urlencoded strings
function cgi_decodevar() {
    [ $# -ne 1 ] && return
    local v t h
    # replace all + with whitespace and append %%
    t="${1//+/ }%%"
    while [ ${#t} -gt 0 -a "${t}" != "%" ]; do
        v="${v}${t%%\%*}" # digest up to the first %
        t="${t#*%}"       # remove digested part
        # decode if there is anything to decode and if not at end of string
        if [ ${#t} -gt 0 -a "${t}" != "%" ]; then
            h=${t:0:2} # save first two chars
            t="${t:2}" # remove these
            v="${v}"`echo -e \\\\x${h}` # convert hex to special char
        fi
    done
    # return decoded string
    echo "${v}"
    return
}
 
# routine to get variables from http requests
# usage: cgi_getvars method varname1 [.. varnameN]
# method is either GET or POST or BOTH
# the magic varible name ALL gets everything
function cgi_getvars() {
    [ $# -lt 2 ] && return
    local q="" p k v s
    # get query
    case $1 in
        GET)
            [ ! -z "${QUERY_STRING}" ] && q="${QUERY_STRING}&"
            ;;
        POST)
            cgi_get_POST_vars
            [ ! -z "${QUERY_STRING_POST}" ] && q="${QUERY_STRING_POST}&"
            ;;
        BOTH)
            [ ! -z "${QUERY_STRING}" ] && q="${QUERY_STRING}&"
            cgi_get_POST_vars
            [ ! -z "${QUERY_STRING_POST}" ] && q="${q}${QUERY_STRING_POST}&"
            ;;
    esac
    shift
    s=" $* "

    # parse the query data
		p="${q%%?*}"  # get first part of query string
    while [ ! -z "$q" ]; do
				q="${q#$p&*}" # strip first part from query string
        k="${q%%=*}"  # get the key (variable name) from it
        v="${q#*=}"   # get the value from it
        v="${v%%&*}"   # get the value from it
        q="${q#$k=$v}" # strip first part from query string
        # decode and evaluate var if requested
        [ "$1" = "ALL" -o "${s/ $k /}" != "$s" ] && \
            eval "$k=\"$(cgi_decodevar $v | sed 's/[\"\\]/\\&/g')\""
    done
    return
}
