#!/usr/bin/python3
## script to get metadata from debsources database and/or other sources
## returns list of sloc for languages in the form [ansic (C), cpp (C++), asm (assembly), 
## java, python, perl, shell script)]
## We can take into account any additional languages
## tries to take the latest version of the package into account (does this make sense?)
def fixName(pkg_name):
    if pkg_name == 'firefox':
        return 'iceweasel'
    else:
        return pkg_name

def getsloccount(cur, pkg_name):

    pkg_name = fixName(pkg_name)

    querry = "SELECT SUM(int_value), st_value FROM metrics WHERE metric_info_id = 'sloccount' AND file_id IN (SELECT file_id from paths WHERE package_id = (SELECT package_id from package_info WHERE package_version = (SELECT MAX(package_version) FROM package_info WHERE package_name = '" + pkg_name + "') AND package_name = '" + pkg_name + "')) GROUP BY st_value ORDER BY st_value;"

    cur.execute(querry)

    rows = cur.fetchall()

    sloc = [0, 0, 0, 0, 0, 0, 0]

    total = 0

    for row in rows:
        total += row[0]
        if row[1] == 'ansic':
            sloc[0] = row[0]
        elif row[1] == 'cpp':
            sloc[1] = row[0]
        elif row[1] == 'asm':
            sloc[2] = row[0]
        elif row[1] == 'java':
            sloc[3] = row[0]
        elif row[1] == 'python':
            sloc[4] = row[0]
        elif row[1] == 'perl':
            sloc[5] = row[0]
        elif row[1] == 'sh':
            sloc[6] = row[0]
            
    return (total, sloc)

if __name__ == "__main__":
    sloc = []
    total = 0
    (total, sloc) = getsloccount('less')
    print(total)
    print(sloc)
